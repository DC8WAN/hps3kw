#ifndef _powrsup_h_
#define _powrsup_h_
int majorVersion;
int minorVersion; 
int controlStatusRegister;

struct SensorsTemp_S
{
  byte Wert;
  byte erhoert;
  byte Fehler;
};
struct AnalogSensors_S
{
  int strom;
  int strom_max;
  int strom_min;
  short spannung_netz;
  SensorsTemp_S temperatur_1;
  SensorsTemp_S temperatur_2;
}
/*  Control Status Register
   BIT 7      BIT 6    BIT 5   BIT 4          BIT 3         BIT 2    BIT 1    BIT 0
   PSON_STAT  BAD_CAL  FAN_HI  SELFTEST_FAIL  ROUT_DISABLE  OC_TRIP  OV_TRIP  OT_TRIP
   R          R        R/W     R              R/W           R        R        R
*/
AnalogSensors;

#define OVER_IN_TEM             0
#define OV_TRIP                 1
#define OC_TRIP                 2
#define OUTPUT_DISABLE_BIT      3
#define FAN_HI_BIT              5
#define PSON_STAT               7

#endif
