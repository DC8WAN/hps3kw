// Touch screen library with X Y and Z (pressure) readings as well
// as oversampling to avoid 'bouncing'
// This demo code returns raw readings, public domain

#include <stdint.h>
#include "TouchScreen.h"

#define YP A0  // must be an analog pin, use "An" notation!
#define XM A1  // must be an analog pin, use "An" notation!
#define YM 8   // can be a digital pin
#define XP 7   // can be a digital pin


// For better pressure precision, we need to know the resistance
// between X+ and X- Use any multimeter to read it
// For the one we're using, its 300 ohms across the X plate
TouchScreen ts = TouchScreen(XP, YP, XM, YM, 300);

void touch_setup() {
//Pipser für Tastentöne an Apin5
pinMode(5, OUTPUT); 
digitalWrite(5, LOW);
  
}

void touch_loop() {
  // a point object holds x y and z coordinates
  TSPoint p = ts.getPoint();
  
  // we have some minimum pressure we consider 'valid'
  // pressure of 0 means no pressing!
  if (p.z > ts.pressureThreshhold) {
     //Serial.print("X = "); Serial.print(p.x);
     //Serial.print("\tY = "); Serial.print(p.y);
     //Serial.print("\tPressure = "); Serial.println(p.z);

     //EXIT zurück zur normalen Ansicht
     if (p.x > 150 && p.x < 260 && p.y < 930 && p.y > 803){

        sh_volt  = 0;
        sh_amp  = 0;
        sh_watt  = 0;
        sh_big = 0;
        sh_change = 1;
        digitalWrite(5, HIGH);
     }
     
     //VOLT in gross anzeigen
     if (p.x > 150 && p.x < 260 && p.y < 775 && p.y > 660){

        sh_volt  = 1;
        sh_change = 1;
        sh_big = 1;
        sh_watt  = 0;
        sh_amp  = 0;
        digitalWrite(5, HIGH);
     }

     //AMP in gross anzeigen
     if (p.x > 150 && p.x < 260 && p.y < 630 && p.y > 520){

        sh_amp  = 1;
        sh_change = 1;
        sh_big = 1;
        sh_volt  = 0;
        sh_watt  = 0;
        digitalWrite(5, HIGH);
     }
     
     //WATT in gross anzeigen
     if (p.x > 150 && p.x < 260 && p.y < 480 && p.y > 370){

        sh_watt  = 1;
        sh_change = 1;
        sh_big = 1;
        sh_volt  = 0;
        sh_amp  = 0;
        digitalWrite(5, HIGH);
     }
     //FAN ON OFF
     if (p.x > 150 && p.x < 260 && p.y < 340 && p.y > 230){
      if (fanon_t){
        fanon_t = 0;
        digitalWrite(5, HIGH);
      }
      else{
        fanon_t  = 1;
        digitalWrite(5, HIGH);
      }
     }
     //Netzteil ON OFF
     if (p.x > 150 && p.x < 260 && p.y < 200 && p.y > 85){
      if (pson_t == 1){
        pson_t = 0;
        digitalWrite(5, HIGH);
      }
      else{
        pson_t = 1;
        digitalWrite(5, HIGH);
      }
   //   Serial.print("\tON/OFF ");
    //  Serial.print(pson_t); Serial.print(" ");
      
     }
     delay(100);
     digitalWrite(5, LOW);
     delay(800);
     
  }
}
