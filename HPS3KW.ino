/*
   Dieser Code läuft auf Arduino Uno und Micro nicht auf Mini, da dieser nicht genug speicher hat
   Verbindungen vom Display und Netzteil zum Arduino

   Display Pin      Arduino Pin
      X- ...... A1
      Y- ...... 8 Digital
      X+ ...... 7 Digital
      Y+ ...... A0
      D/C ..... 9
      CS ...... 10
      MOSI .... 11
      MISO .... 12
      CLK ..... 13
      3-5V .... 5V
      GND ..... GND

   Netzteilanschluss. Blick auf die Buchse, Platiene unten
   --------------------------------------------------------------|
   |___        1       2         3           4         5     ____|
   |   |  A  (5V+)   5V RNT     PSON        SCL        A0   |    |
   |___|  B   5V+    5V RNT    RESENT   SCL/SDA RNT    A1   |____|
   |      C   5V+    5V RNT     PSKI        SDA        A2        |
   |      D   NC       NC       PWOK        FAIL       A3        |Platiene
   --------------------------------------------------------------|--------------

   PSON+RESENT+PSKI brücken um das Netzteil einzuschalten.
   Dies führt aber beim einschalten des Netzeils dazu, dass bevor der Arduino sich verbunden hat das Netzteil kurtz Spannung lifert.
   Problem kann so gelößt werden:
   PSON+RESENT+PSKI erst miteinem Transistor bc547 und Pin 6 am Arduino zusammengeschalten, wenn man auf ON drückt und der Pin 6 auf HIGH geht.
   PSON + PSKI ->> Collector (3)
   RESENT auf  ->> Emitter (1)
   Pin 6 am Arduino über 500Ohm ->> Base (2)


    Netzteil       Arduino
    SLC .......... A5
    SCL/SDA RNT .. GND and GND for Vin
    SDA .......... A4
    (5V+)......... Vin

    Sonstige Pins
    D5 ....... +Signalgeber bei Touch
    D6 ....... PS_ON an der Transistorbasis
    A3 ....... Spannung messen über Teiler (Da unterschiedliche GND nicht genau

    Infos unter https://wiki.dm6a.de/books/endstufe-1kw/page/3kw-netzteil
    Dieses Projeckt wurde von DK8DE und DC8WAN erstellt.
*/
//long i = 1;
int pson_t = 0; //Netzteil wird mit 0 ausgeschaltet und mit 1 ein
int fanon_t = 0; //Lüfter regeln 0 normal 1 full
int sh_watt = 0; //zeigt die Watt in gross 0 ist normale Ansicht
int sh_amp = 0; //zeigt die AMP in gross 0 ist normale Ansicht
int sh_volt = 0; //zeigt die VOLT in gross 0 ist normale Ansicht
int sh_big = 0; //wenn 1 dann werden die beiden mittigen Trennlinien entfernt für grosse Ansicht. Die Balken und Messwerte werden mit ausgeblendet
int sh_change = 1; //wird auf 1 gesetzt beim Seitenwechsel um Linien nur einmal zu ändern. Mit 3 wird die Anzeige auf kleine Ansicht zurück gestellt und bei 0 ändert sich nichts
int intro = 1;
float MapFiltVal = 0;
double x_delay_graph = 0;
int redraw = true;


void setup() {
  delay(2000); //debug!! muß bleiben da sonst der Display nicht gedreht wird.
  Serial.begin(9600);
  pwrsup_setup();
  display_main_setup();
  touch_setup();

  //Anzeige wird nur einmal in der Sekunde neu aufgebaut rest läuft im Tackt
  cli(); // disable interrupts
  // reset
  TCCR1A = 0; // set TCCR1A register to 0
  TCCR1B = 0; // set TCCR1B register to 0
  TCNT1  = 0; // reset counter value
  OCR1A = 31249; // compare match register
  // set prescaler
  TCCR1B |= (1 << CS12) | (1 << CS10);
  TCCR1B |= (1 << WGM12); // turn on CTC mode
  TIMSK1 |= (1 << OCIE1A); // enable timer compare interrupt
  sei(); // allow interrupts
}


ISR(TIMER1_COMPA_vect) { // function which will be called when an interrupt occurs at timer 1
  x_delay_graph += 1;  // Einmal 1/s wird der Graph aktualisiert und nach 60 sec Reset auf 0
}
void loop() {
  display_main_loop();
  //int i++;
  pwrsup_loop();
  touch_loop();

}
