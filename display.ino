#include <SPI.h>
#include "Adafruit_GFX.h"
#include "Adafruit_HX8357.h"
#include "pwrsup.h"

// These are 'flexible' lines that can be changed
#define TFT_CS 10
#define TFT_DC 9
#define TFT_RST -1 // War mal auf 8 RST can be set to -1 if you tie it to Arduino's reset

// Colors
#define WHITE     0xFFFF
#define BLACK     0x0000
#define YELLOW    0xFFE0
#define DKGREEN   0x03E0
#define GREY      0xA618 //bissel blau 

#define RED      0xF800
#define GREEN    0x07E0
#define TEXTCOLOR 0xFFFF
#define BGCOLOR  0x0000

#define BUTCOLOR1 0x7BCF
#define BUTCCOLOR 0x7BFF

#define BUBORDER1 0xCE59
#define BUBORDER2 0x39E7
//#define BUFONTCOL 0xFFFF

#define BARGREEN  0x07E0
//#define BARYELLOW 0xFFE0
#define BARRED  0xF800

//#define BARBG 0x0000
#define BARACT 0x07E0 //0x0AF9

#include <Fonts/FreeSans24pt7b.h>
#include <Fonts/FreeSans9pt7b.h>

float AlteSpannung = 1;
float AlteLeistung = 1;
float AlterStrom = 1;
int FontToggleGreen = 0; //Schaltet die Farbe auf Gruen beim aktiven Graphen für A V W über dem Graphen. 1=V 2=A 3=W 0=Std.
int FONT_COLOR = 0xFFFF;
int temp1 = 1;
int temp2 = 1;
float netz = 220;
float netz_alt = 1;
int FAL = 3;
int sw_on_off = 0;
int sw_on_off_alt = 0;
int fan_on_off = 0;
int fan_on_off_alt = 0;
String fault = "Normal";
int fault_alt = 1;
int no_fault = 1;
boolean display1 = true;
double ox , oy ;
double x, y;




// Use hardware SPI (on Uno, #13, #12, #11) and the above for CS/DC
Adafruit_HX8357 tft = Adafruit_HX8357(TFT_CS, TFT_DC, TFT_RST);

void display_main_setup()
{

  tft.begin(HX8357D);

  //Display Drehen
  tft.setRotation(3);

  //Intro DK8DE
  //Schwarzer Bildschirm zum Start
  tft.fillScreen(BGCOLOR);
  //Outline Display
  tft.drawRect(0, 0, 480, 320, BUBORDER1);
  tft.drawRect(1, 1, 478, 318, BUBORDER1);
  tft.setCursor(55, 135);
  tft.setFont(&FreeSans24pt7b);
  tft.setTextSize(1);
  tft.setTextColor(TEXTCOLOR);
  tft.print("50V 3000W PSU");
  tft.setCursor(165, 200);
  tft.print("DK8DE");
  tft.setTextSize(0);
  delay(3000);

  //Trennlinien
  tft.drawLine(2, 64, 480, 64, BUBORDER1);
  tft.drawLine(2, 256, 480, 256, BUBORDER1);

  //BU Exit
  drbu(10, 10, 65, 46);
  tft.setCursor(23, 38);
  tft.setTextColor(TEXTCOLOR);
  tft.setFont(&FreeSans9pt7b);
  tft.print("EXIT");

  //BU Volt
  drbu(89, 10, 65, 46);
  tft.setCursor(98, 38);
  tft.print("VOLT");

  //BU AMP
  drbu(168, 10, 65, 46);
  tft.setCursor(182, 38);
  tft.print("AMP");

  //BU Watt
  drbu(247, 10, 65, 46);
  tft.setCursor(254, 38);
  tft.print("WATT");

  //BU FAN
  drbu(326, 10, 65, 46);

  //BU ON
  drbu(405, 10, 65, 46);

  //Main Line V
  tft.setCursor(20, 280);
  tft.setTextColor(TEXTCOLOR);
  tft.setFont(&FreeSans9pt7b);
  tft.print("Line:");

  //Power ON / OFF
  tft.setCursor(20, 303);
  tft.print("Power:");

  //Temp In
  tft.setCursor(175, 280);
  tft.setTextColor(TEXTCOLOR);
  tft.print("Temp IN:");
  tft.setTextColor(GREEN);
  tft.setCursor(297, 280);
  tft.print("C");

  //Temp OUT
  tft.setCursor(175, 303);
  tft.setTextColor(TEXTCOLOR);
  tft.print("Temp OUT:");
  tft.setTextColor(GREEN);
  tft.setCursor(297, 303);
  tft.print("C");

  //Fan Normal
  tft.setCursor(330, 280);
  tft.setTextColor(TEXTCOLOR);
  tft.print("Fan:");


  //Status / Fehler
  tft.setCursor(330, 303);
  tft.print("Err:");

}

//Funktion um die Buttons zu Zeichnen
void drbu(int A1, int A2, int A3, int A4)
{
  //tft.drawRect(A1 , A2, A3 + 1, A4 + 1, BUBORDER2);
  tft.drawRect(A1 - 1, A2 - 1 , A3, A4, BUBORDER1);
  tft.fillRect(A1, A2, A3, A4, BUTCOLOR1);
}



void display_main_loop()
{
  //Sammeln und berechnen von Messdaten
  float Strom = ((float)AnalogSensors.strom) * 0.001;
  float Spannung = MapFiltVal;
  float Leistung = Strom * Spannung;
  netz = ((float)AnalogSensors.spannung_netz) / 100, 2;
  netz = netz + 10;

  //Main Line V
  if (netz_alt != netz)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setCursor(81, 280);
    tft.setTextColor(BGCOLOR);
    tft.print(netz_alt);
    tft.setCursor(81, 280);
    tft.setTextColor(GREEN);
    tft.print(netz);
    netz_alt = netz;
    tft.setCursor(135, 280);
    tft.print(" V");
  }
  //Werte auf 1 damit wenn keine Änderung da ist die Zahlen auch dargestellt werden
  if (sh_change == 1) {
    AlteSpannung = 1;
    AlterStrom = 1;
    AlteLeistung = 1;
  }

  //Power ON / OFF
  if (controlStatusRegister & (1 << OUTPUT_DISABLE_BIT)) {
    sw_on_off = 0;
  }
  else {
    sw_on_off = 1;
  }

  if (sw_on_off == 0 & sw_on_off_alt != sw_on_off)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setTextColor(BGCOLOR);
    tft.setCursor(81, 303);
    tft.print("ON");
    tft.setCursor(81, 303);
    tft.setTextColor(RED);
    tft.print("OFF");
    tft.setTextColor(TEXTCOLOR);
    tft.setCursor(424, 38);
    tft.print("ON");
    sw_on_off_alt = sw_on_off;
  }
  if (sw_on_off == 1 & sw_on_off_alt != sw_on_off)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setCursor(81, 303);
    tft.setTextColor(BGCOLOR);
    tft.print("OFF");
    tft.setCursor(81, 303);
    tft.setTextColor(GREEN);
    tft.print("ON");
    //BU ON = ON
    tft.setCursor(424, 38);
    tft.setTextColor(GREEN);
    tft.print("ON");
    sw_on_off_alt = sw_on_off;
    no_fault = 1; //Setzt den Fehler zurück, sollte das Netzteil durch einen Fehler abgeschaltet worden sein

  }


  //Temp In
  if (AnalogSensors.temperatur_1.Wert != temp1)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setCursor(275, 280);
    tft.setTextColor(BGCOLOR);
    tft.print(temp1);
    tft.setCursor(275, 280);
    tft.setTextColor(GREEN);
    tft.print(AnalogSensors.temperatur_1.Wert);
    temp1 = AnalogSensors.temperatur_1.Wert;
  }


  //Temp OUT
  if (AnalogSensors.temperatur_2.Wert != temp2)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setCursor(275, 303);
    tft.setTextColor(BGCOLOR);
    tft.print(temp2);
    tft.setCursor(275, 303);
    tft.setTextColor(GREEN);
    tft.print(AnalogSensors.temperatur_2.Wert);
    temp2 = AnalogSensors.temperatur_2.Wert;
  }

  if (controlStatusRegister &  (1 << FAN_HI_BIT)) {
    fan_on_off = 0;
  }
  else {
    fan_on_off = 1;
  }
  //Fan
  if (fan_on_off == 0 & fan_on_off_alt != fan_on_off)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setCursor(375, 280);
    tft.setTextColor(BGCOLOR);
    tft.print("Normal");
    tft.setTextColor(RED);
    tft.setCursor(375, 280);
    tft.print("Full");
    tft.setCursor(341, 38);
    tft.setTextColor(GREEN);
    tft.print("FAN");
    fan_on_off_alt = fan_on_off;
  }
  if (fan_on_off == 1 & fan_on_off_alt != fan_on_off)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setCursor(375, 280);
    tft.setTextColor(BGCOLOR);
    tft.print("Full");
    tft.setTextColor(GREEN);
    tft.setCursor(375, 280);
    tft.print("Normal");
    tft.setCursor(341, 38);
    tft.setTextColor(TEXTCOLOR);
    tft.print("FAN");
    fan_on_off_alt = fan_on_off;
  }

  //Status / Fehler
  if ( controlStatusRegister &  (1 << OVER_IN_TEM) ) {
    fault = "OT FAULT";
    fault_alt = 1;
    no_fault = 0;
    pson_t = 0; //Schaltet beim Fehler das Netzteil aus
  }
  else if (controlStatusRegister &  (1 << OV_TRIP) ) {
    fault = "OV FAULT";
    fault_alt = 1;
    no_fault = 0;
    pson_t = 0; //Schaltet beim Fehler das Netzteil aus
  }
  else if (controlStatusRegister &  (1 << OC_TRIP) ) {
    fault = "OC FAULT";
    fault_alt = 1;
    no_fault = 0;
    pson_t = 0; //Schaltet beim Fehler das Netzteil aus
  }
  else {
    fault_alt = 0;
    fault = "Normal";
  }
  if (fault_alt == 1)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.setCursor(375, 303);
    tft.setTextColor(BGCOLOR);
    tft.print("Normal");
    tft.setCursor(375, 303);
    tft.setTextColor(RED);
    tft.print(fault);
  }
  else if (no_fault == 1)
  {
    tft.setFont(&FreeSans9pt7b);
    tft.fillRect(370, 285, 105, 25, BGCOLOR);
    tft.setCursor(375, 303);
    tft.setTextColor(GREEN);
    tft.print("Normal");
    fault_alt = 0;
    no_fault = 0;
  }
  //Anzeige in der Mitte ueberschreiben und Zahlen in gross anzeigen

  if (sh_change == 1) {
    tft.fillRect(9, 65, 466, 184, BGCOLOR);
  }
  if (sh_big != 1) {
    //Sensor U I P
    //V
    if (Spannung != AlteSpannung)
    {
      tft.setFont(&FreeSans24pt7b);
      tft.setCursor(250, 111);
      tft.setTextColor(BGCOLOR);
      tft.print(AlteSpannung);
      tft.setCursor(250, 111);
      tft.setTextColor(TEXTCOLOR);
      tft.print(Spannung);
      AlteSpannung = Spannung;
    }

    //A
    if (Strom != AlterStrom)
    {
      tft.setFont(&FreeSans24pt7b);
      tft.setCursor(250, 175);
      tft.setTextColor(BGCOLOR);
      tft.print(AlterStrom);
      tft.setCursor(250, 175);
      tft.setTextColor(TEXTCOLOR);
      tft.print(Strom);
      AlterStrom = Strom;
    }

    //WATT
    if (Leistung != AlteLeistung)
    {
      tft.setCursor(250, 239);
      tft.setFont(&FreeSans24pt7b);
      tft.setTextColor(BGCOLOR);
      tft.print(AlteLeistung);
      tft.setCursor(250, 239);
      tft.setTextColor(TEXTCOLOR);
      tft.print(Leistung);
      AlteLeistung = Leistung;
    }
    //Bar V1
    //      tft.fillRect(10, 80, 179, 15, BARRED);
    //      tft.fillRect(189, 80, 23, 15, BARYELLOW);
    //      tft.fillRect(212, 80, 23, 15, BARGREEN);
    tft.drawRect(9, 80, 227, 34, BUBORDER1);

    //Bar I1
    //      tft.fillRect(10, 144, 179, 15, BARGREEN);
    //      tft.fillRect(189, 144, 23, 15, BARYELLOW);
    //      tft.fillRect(212, 144, 23, 15, BARRED);
    tft.drawRect(9, 144, 227, 34, BUBORDER1);

    //Bar W1
    //      tft.fillRect(10, 208, 179, 15, BARGREEN);
    //      tft.fillRect(189, 208, 23, 15, BARYELLOW);
    //      tft.fillRect(212, 208, 23, 15, BARRED);
    tft.drawRect(9, 208, 227, 34, BUBORDER1);

    //Bar V2
    tft.fillRect(10 + (((float)Spannung / 52) * 225), 81, 225 - (((float)Spannung / 52) * 225), 32, BLACK);
    tft.fillRect(10, 81, (((float)Spannung / 52) * 225), 32, BARACT);   //der soll sich bewegen bzw. Läge ändern für U.

    //Bar I2
    tft.fillRect(10 + (((float)Strom / 57) * 225), 145, 225 - (((float)Strom / 57) * 225), 32, BLACK);
    tft.fillRect(10, 145, (((float)Strom / 57) * 225), 32, BARACT);   //der soll sich bewegen bzw. Läge ändern für I

    //Bar W2
    tft.fillRect(10 + (((float)Leistung / 2950) * 209), 209, 225 - (((float)Leistung / 2950) * 225), 32, BLACK);
    tft.fillRect(10, 209, (((float)Leistung / 2950) * 209), 32, BARACT);    //der soll sich bewegen bzw. Läge ändern für P

    //Symbol rechts mit U / I / P
    tft.setTextColor(TEXTCOLOR);
    tft.setFont(&FreeSans24pt7b);
    tft.setCursor(430, 111);
    tft.print("V");

    //A
    tft.setCursor(430, 175);
    tft.print("A");

    //WATT
    tft.setCursor(424, 239);
    tft.print("W");
  }
  else {
    /*
       Graph neu aufbauen wenn 60 sec vergangen sind
    */
    if (x_delay_graph >= 60 || sh_change == 1) {
      display1 = true; //Anzeige zurücksetzen
      x_delay_graph = 0; //Sekundenzähler zurücksetzen
      x = 0;
      y = 0;
      ox = 0;
      oy = 0;
    }
    else {
      x = x_delay_graph;
    }
    //VOLT in Groß darstellen
    if (sh_volt == 1) {
      /*
        Graph darstellen für Strom. Nach 60 Sec wird er neu aufgebaut
      */
      y = Spannung; //Keine anpassung der Scala da nur max 55V
      FontToggleGreen = 1;
      Graph(x, y, 50, 230, 385, 130, 0, 60, 10, 0, 55, 5, "", "", "VOLT", GREY, WHITE, YELLOW, WHITE, BLACK, display1);
    }
    //AMP in Groß darstellen
    if (sh_amp == 1) {
      /*
        Graph darstellen für Strom. Nach 60 Sec wird er neu aufgebaut
      */
      y = Strom; //Keine anpassung der Scala da nur max 60A
      FontToggleGreen = 2;
      Graph(x, y, 50, 230, 385, 130, 0, 60, 10, 0, 60, 5, "", "", "AMP", GREY, WHITE, YELLOW, WHITE, BLACK, display1);
    }
    if (sh_watt == 1) {
      /*
         Graph darstellen für Watt. Nach 60 Sec wird er neu aufgebaut
      */
      y = Leistung / 1000; //anpassen an die Scala bis 3.0 KW geht in 0.25 Schritten
      FontToggleGreen = 3;
      Graph(x, y, 50, 230, 385, 130, 0, 60, 10, 0, 3, 0.25, "", "", "KW", GREY, WHITE, YELLOW, WHITE, BLACK, display1);
    }
    /*
       Volt Amp und Watt im über dem Graphen darstellen
    */
    if (Spannung != AlteSpannung)
    {
      if (FontToggleGreen == 1) {
        FONT_COLOR = 0x07E0;
      } else {
        FONT_COLOR = 0xFFFF;
      }
      tft.setFont(&FreeSans9pt7b);
      tft.setTextSize(0);
      tft.setTextColor(BGCOLOR);
      tft.setCursor(110, 89);
      tft.print(AlteSpannung);
      tft.setTextColor(FONT_COLOR);
      tft.setCursor(90, 89);
      tft.print("V");
      tft.setCursor(110, 89);
      tft.print(Spannung);

      AlteSpannung = Spannung;
    }
    if (Strom != AlterStrom) {
            if (FontToggleGreen == 2) {
        FONT_COLOR = 0x07E0;
      } else {
        FONT_COLOR = 0xFFFF;
      }
      tft.setFont(&FreeSans9pt7b);
      tft.setTextSize(0);
      tft.setTextColor(FONT_COLOR);
      tft.setCursor(170, 89);
      tft.print("A");
      tft.setCursor(185, 89);
      tft.setTextColor(BGCOLOR);
      tft.print(AlterStrom);
      tft.setCursor(185, 89);
      tft.setTextColor(FONT_COLOR);
      tft.print(Strom);
      AlterStrom = Strom;
    }
    if (Leistung != AlteLeistung) {
            if (FontToggleGreen == 3) {
        FONT_COLOR = 0x07E0;
      } else {
        FONT_COLOR = 0xFFFF;
      }
      tft.setFont(&FreeSans9pt7b);
      tft.setTextColor(FONT_COLOR);
      tft.setTextSize(0);
      tft.setCursor(250, 89);
      tft.print("W");
      tft.setCursor(270, 89);
      tft.setTextColor(BGCOLOR);
      tft.print(AlteLeistung);
      tft.setCursor(270, 89);
      tft.setTextColor(FONT_COLOR);
      tft.print(Leistung);

      AlteLeistung = Leistung;
    }
    FONT_COLOR = 0xFFFF;
  }
  sh_change = 0;
}
