#include "i2cmaster.h"
#include "pwrsup.h"
//float FVal;

float FiltVal = 911.00;
int j;

// I2C Library, mit Erweiterung
// Arduino analog input 4 = I2C SDA
// Arduino analog input 5 = I2C SCL

#define I2C_CPU_ADDR         B0011111 // Highes 7Bit for the Address, the last bit is R/W Flag and added by the lib 
#define I2C_FRU_EEPROM_ADDR  B1010111 // Highest 7Bit of the Address, the last bit is R/W Flag and added by the lib
#define I2C_BROADCAST_ADDR   B0000000 // Broadcast Adresse
#define I2C_MESSAGE_DELAY    50       // minimum delay time between i2c requests

int slaveAddress;
byte bVal;

// CommandIDs for AA21970
#define HP3KWPS_SCRC  0x01 // (Set_Control_Register_Command)
#define HP3KWPS_RSRC  0x02 // (Read_Status_Register_Command)
#define HP3KWPS_RASDC 0x03 // (Read_Analog_Sensor_Data_Command)


// CommandIDs for AA21970 and HP3KW
#define HP3KWPS_TMC   0x04 //(Test_Mode_Command)
#define HP3KWPS_FDC   0x05 // (Firmware_Debug_Command)
#define HP3KWPS_FRNC  0x06 // (Firmware_Revision_Number_Command)
#define HP3KWPS_STMC  0x07 // (System_Test_Mode_Command)
#define HP3KWPS_STDFC 0x08 // (System_Test_Data_Format_Command)
#define HP3KWPS RRAMC 0x09 // (Read_RAM_Command)
#define HP3KWPS_RSFRC 0x0A // (Read_SFR_Command)


void initI2C() {
  i2c_init();          // join i2c bus
  slaveAddress = I2C_CPU_ADDR << 1; // Prepare Slave Adressbyte
}

void reinitI2C() {
  i2c_disableTWI();
  delay(1);
  i2c_init();          // join i2c bus
  slaveAddress = I2C_CPU_ADDR << 1; // Prepare Slave Adressbyte
}

/*


  Control Status Register
   BIT 7      BIT 6    BIT 5   BIT 4          BIT 3         BIT 2    BIT 1    BIT 0
   PSON_STAT  BAD_CAL  FAN_HI  SELFTEST_FAIL  ROUT_DISABLE  OC_TRIP  OV_TRIP  OT_TRIP
   R          R        R/W     R              R/W           R        R        R

  PSON_STAT: This bit is an image of the PSON# signal coming into the power supply from the system.
  BAD_CAL: This bit will be set to indicate a corrupted calibration table. Under this condition the response
  to a request for analog data shall be all zeros.
  FAN_HI: Setting this bit will cause the fan run full speed. Reading this bit will tell if the fan is running at
  full speed due to internal temperatures or in response to an I2C command.
  SELFTEST_FAIL: This bit will be set to indicate a power supply self-test failure. Under this condition the
  response to a request for analog data shall be all zeros.
  ROUT_DISBLE: Remote Output Disable. Setting this bit will disable the main outputs and clearing this bit
  will enable the main output.
  OC_TRIP: Over current trip status. This bit is set when the power supply output has been disabled due to
  an over current event. In this condition, the Inlet Air temperature will be sensed only when the Amber LED is
  off (Every other second) and the ADC update flag will be set according to the other three ADC channel
  reading status.
  OV_TRIP: Over voltage trip status. This bit will be set when the power supply outputs have been
  disabled and the power supply temperature has not reached the trip limits. This is not a direct sensing of the
  over voltage trip circuit.
  OT_TRIP: Over temperature status. This bit will be set when the power supply outputs have been
  disabled and the power supply temperature is past the trip limit. This is not a direct sensing of the over
  temperature trip circuit.


  void printControlStatusRegister() {
  //Serial.print("CtrlReg:\t");
  Serial.println(controlStatusRegister, BIN);
  }
*/
void readControlStatusRegister() {
  bVal = i2c_start(slaveAddress + I2C_WRITE); // set device address and write mode
  if (bVal) {
    reinitI2C();
 //   Serial.println("start: netzteil not ready 1 Konnte Byte zum Schreiben nicht setzen");
    return;
  }
  i2c_write(HP3KWPS_RSRC);                 // write a databyte to i2c device (command)

  bVal = i2c_rep_start(slaveAddress + I2C_READ); // set same device address and define read mode
  if (bVal) {
    reinitI2C();
  //  Serial.println("repstart: netzteil not ready 2 Konnte Byte zum Lesen nicht setzen");
    return;
  }
  controlStatusRegister = i2c_readNak();              // read one byte
  i2c_stop();
}

void setControlStatusRegister(int valueByte) {
  bVal = i2c_start(slaveAddress + I2C_WRITE); // set device address and write mode
  if (bVal) {
    reinitI2C();
  //  Serial.println("start: netzteil not ready 3 Konnte Byte für Schreibmodus nicht setzen");
    return;
  }
  i2c_write(HP3KWPS_SCRC);                 // write a databyte to i2c device (command)
  i2c_write(valueByte);                  // write a databyte to i2c device (command)
  i2c_write(valueByte);                  // write a databyte to i2c device (command)
  i2c_write(valueByte + valueByte);                  // write a databyte to i2c device (command)
  i2c_stop();
}

void readAnalogSensors() {
  bVal = i2c_start(slaveAddress + I2C_WRITE); // set device address and write mode
  if (bVal) {
    reinitI2C();
    //Serial.println("start: netzteil not ready");
    AnalogSensors.strom = 0;
    AnalogSensors.strom_max = 0;
    AnalogSensors.strom_min = 0;
    AnalogSensors.spannung_netz = 0;
    AnalogSensors.temperatur_1.Wert = 0;
    AnalogSensors.temperatur_1.erhoert = 0;
    AnalogSensors.temperatur_1.Fehler = 0;
    AnalogSensors.temperatur_2.Wert = 0;
    AnalogSensors.temperatur_2.erhoert = 0;
    AnalogSensors.temperatur_2.Fehler = 0;
    return;
  }
  i2c_write(HP3KWPS_RASDC);                 // write a databyte to i2c device (command)
  bVal = i2c_rep_start(slaveAddress + I2C_READ); // set same device address and define read mode
  if (bVal) {
    reinitI2C();
    AnalogSensors.strom = 0;
    AnalogSensors.strom_max = 0;
    AnalogSensors.strom_min = 0;
    AnalogSensors.spannung_netz = 0;
    AnalogSensors.temperatur_1.Wert = 0;
   // AnalogSensors.temperatur_1.erhoert = 0;
    AnalogSensors.temperatur_1.Fehler = 0;
    AnalogSensors.temperatur_2.Wert = 0;
   // AnalogSensors.temperatur_2.erhoert = 0;
    AnalogSensors.temperatur_2.Fehler = 0;
   // Serial.println("repstart: netzteil not ready 4 Fehler lesen der analogen Sensoren");
    return;
  }

  int analogSensor[18];
  for (int i = 0; i < 17; i++) {
    analogSensor[i] = i2c_readAck();
  }
  analogSensor[17] = i2c_readNak();
  i2c_stop();

  if (analogSensor[17] != 0)
  {
    AnalogSensors.strom = ((int)analogSensor[0] + analogSensor[1] * 256 + analogSensor[2] * 65335);
    AnalogSensors.strom_max = ((unsigned int)analogSensor[3] + analogSensor[4] * 256 + analogSensor[5] * 65335);
    AnalogSensors.strom_min = ((int)analogSensor[6] + analogSensor[7] * 256 + analogSensor[8] * 65335);
    AnalogSensors.spannung_netz = ((float)analogSensor[9] + analogSensor[10] * 256);
    AnalogSensors.temperatur_1.Wert = analogSensor[11];
    //AnalogSensors.temperatur_1.erhoert = analogSensor[12];
    AnalogSensors.temperatur_1.Fehler = analogSensor[13];
    AnalogSensors.temperatur_2.Wert = analogSensor[14];
   // AnalogSensors.temperatur_2.erhoert = analogSensor[15];
    AnalogSensors.temperatur_2.Fehler = analogSensor[16];
  }
}
//Werte glätten für die Anzeige
void Filtern(float &FiltVal, int NewVal, int FF) {
  FiltVal = ((FiltVal * FF) + NewVal) / (FF + 1);
}

void pwrsup_setup()
{
  pinMode(6, OUTPUT); //Netzteil PS ON .. Brücke mit Transistor durchschalten. Netzteil geht sonst an bevor der Aruino oben ist und es wieder abschaltet
  digitalWrite(6, LOW);
  initI2C(); //Initialise the i2c bus mit verbindung zu HP Netzteil
}


void pwrsup_loop()
{
  //Spannung Messen am Teiler
  if (pson_t == 1) { //nur messen wenn das Netzteil an ist
   // int Usensor = analogRead(A3);
   // Filtern(FiltVal, Usensor, 10); //Werte glätten
   // Serial.println(FiltVal);
   // MapFiltVal = map(FiltVal, 530.00, 906.00, 0.00, 50.00);
   // Serial.println(MapFiltVal);
   MapFiltVal = 50.00;
  }
  else{ MapFiltVal = 0;}

  readControlStatusRegister();

  // Der Powerausgang aktiviert und deaktivert
  if (pson_t) {
    controlStatusRegister &= ~(1 << OUTPUT_DISABLE_BIT);
    digitalWrite(6, HIGH);
  } else {
    controlStatusRegister |= (1 << OUTPUT_DISABLE_BIT);
    digitalWrite(6, LOW);
  }
  // Luefter auf Vollspeed gesetzt

  if (fanon_t) {
    controlStatusRegister |= (1 << FAN_HI_BIT);
  } else {
    controlStatusRegister &= ~(1 << FAN_HI_BIT);
  }

  setControlStatusRegister(controlStatusRegister); // Enable Power Output
  delay(I2C_MESSAGE_DELAY);

  readAnalogSensors();
}
